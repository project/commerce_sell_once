<?php

namespace Drupal\commerce_sell_once;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock\StockCheckInterface;

/**
 * The stock checker implementation for the sell once module.
 */
class SellOnceChecker implements StockCheckInterface {

  /**
   * {@inheritdoc}
   */
  public function getTotalStockLevel(PurchasableEntityInterface $entity, array $locations) {
    if (!$entity->get('commerce_sell_once_sold') || !$entity->get('commerce_sell_once_sold')->value) {
      return 1;
    }

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalAvailableStockLevel(PurchasableEntityInterface $entity, array $locations) {
    return $this->getTotalStockLevel($entity, $locations);
  }

  /**
   * {@inheritdoc}
   */
  public function getIsInStock(PurchasableEntityInterface $entity, array $locations) {
    return $this->getTotalStockLevel($entity, $locations) > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsAlwaysInStock(PurchasableEntityInterface $entity) {
    return FALSE;
  }

}
