<?php

namespace Drupal\commerce_sell_once;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock\StockServiceConfigInterface;

/**
 * The sell once service configuration class.
 */
class SellOnceServiceConfig implements StockServiceConfigInterface {

  /**
   * {@inheritdoc}
   */
  public function getAvailabilityLocations(Context $context, PurchasableEntityInterface $entity) {
    return [
      new EmptyLocation(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionLocation(Context $context, PurchasableEntityInterface $entity, $quantity) {
    $locations = $this->getAvailabilityLocations($context, $entity);
    return array_shift($locations);
  }

}
