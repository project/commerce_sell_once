<?php

namespace Drupal\commerce_sell_once\Event;

use Drupal\commerce\EventBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines the stock location event.
 */
class SellOnceTransactionEvent extends EventBase {

  /**
   * The stock transaction.
   *
   * @var array
   *   Associative array containing all the values of the transaction.
   *
   * @see \Drupal\commerce_sell_once\SellOnceUpdater::createTransaction()
   */
  protected $stockTransaction;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new stock transaction event.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param array $stock_transaction
   *   The sell once stock transaction.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, array $stock_transaction) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stockTransaction = $stock_transaction;
  }

  /**
   * Get the purchasable entity.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface
   *   The purchasable entity.
   */
  public function getEntity() {
    $entityId = $this->stockTransaction['entity_id'];
    $entityType = $this->stockTransaction['entity_type'];

    return $this->entityTypeManager->getStorage($entityType)
      ->load($entityId);
  }

  /**
   * Get whether the transaction was sold or unsold.
   *
   * @return bool
   *   Whether the transaction was sold or unsold.
   */
  public function getSold() {
    return $this->stockTransaction['sold'];
  }

  /**
   * Get the stock transaction.
   *
   * @return array
   *   The sell once stock transaction.
   */
  public function getStockTransaction() {
    return $this->stockTransaction;
  }

}
