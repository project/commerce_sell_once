<?php

namespace Drupal\commerce_sell_once\EventSubscriber;

use Drupal\commerce_sell_once\Event\SellOnceTransactionEvent;
use Drupal\commerce_sell_once\Event\SellOnceTransactionEvents;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to Commerce Sell Once transaction events.
 */
class CommerceSellOnceTransactionSubscriber implements EventSubscriberInterface {

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a CommerceStockTransactionSubscriber.
   *
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    return [
      SellOnceTransactionEvents::SELL_ONCE_TRANSACTION => 'onTransaction',
    ];
  }

  /**
   * Invalidate the cache for the purchased entity.
   *
   * @param \Drupal\commerce_sell_once\Event\SellOnceTransactionEvent $event
   *   The event.
   */
  public function onTransaction(SellOnceTransactionEvent $event) {
    $purchasableEntity = $event->getEntity();
    $this->cacheTagsInvalidator->invalidateTags($purchasableEntity->getCacheTagsToInvalidate());
  }

}
