# Commerce Sell Once

See Commerce Stock documentation for full setup.

## Install the module

Install with [Composer](https://getcomposer.org/):
```
composer require 'drupal/commerce_sell_once:^1.0'
```

## Setup

1. Enable the following modules
    * Commerce Sell Once
    * Commerce Stock UI
    * Commerce Stock Enforcement (required to restrict purchasing more than once)

2. Commerce >> Configuration >> Stock >> Stock configuration
    * Set Default service to "Sell Once" (optionally select Sell Once only
    for product variations that should be controlled by stock)
