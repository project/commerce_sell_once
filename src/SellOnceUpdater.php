<?php

namespace Drupal\commerce_sell_once;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_sell_once\Event\SellOnceTransactionEvent;
use Drupal\commerce_sell_once\Event\SellOnceTransactionEvents;
use Drupal\commerce_stock\StockUpdateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class SellOnceUpdater.
 */
class SellOnceUpdater implements StockUpdateInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the sell once stock updater.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager) {
    $this->eventDispatcher = $event_dispatcher;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Creates an instance of the sell once stock checker.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The DI container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createTransaction(PurchasableEntityInterface $entity, $location_id, $zone, $quantity, $unit_cost, $currency_code, $transaction_type_id, array $metadata) {
    // Quantity less than zero is removing stock, so mark as sold.
    $sold = $quantity < 0;

    $entity->set('commerce_sell_once_sold', $sold);
    $entity->save();

    $field_values = [
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
      'sold' => $sold,
    ];

    $event = new SellOnceTransactionEvent($this->entityTypeManager, $field_values);

    $this->eventDispatcher->dispatch($event, SellOnceTransactionEvents::SELL_ONCE_TRANSACTION);

    // No actual transaction ID, so return null.
    return NULL;
  }

}
