<?php

namespace Drupal\commerce_sell_once;

use Drupal\commerce_stock\StockLocationInterface;

class EmptyLocation implements StockLocationInterface {

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return null;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return TRUE;
  }

}
