<?php

namespace Drupal\commerce_sell_once\Event;

/**
 * List of sell once stock transaction events.
 */
final class SellOnceTransactionEvents {

  /**
   * Name of the event fired after a stock transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_sell_once\Event\SellOnceTransactionEvent
   */
  const SELL_ONCE_TRANSACTION = 'commerce_sell_once.stock_transaction';

}
